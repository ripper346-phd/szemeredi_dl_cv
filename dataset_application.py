import torch
import random
from torch_geometric.data import DataLoader
from szemeredi_calc import apply_szemeredi, create_plain_dataset
from split import divide_dataset
from train import train
from db import Database
from model import GCN
import numpy


class DatasetApplication:
    def __init__(self, dataset, db_table, batch_size, verbose=False):
        self.verbose = verbose
        self.dataset = dataset
        self.db_table = db_table
        self.batch_size = batch_size
        self.new_dataset = None
        self.db = None
        self.train_loader = None
        self.test_loader = None
        self.train_rate = 0.8
        self.set_db()

    def dataset_stats(self):
        print(f'nr. graphs: {len(self.dataset)}')
        print(f'nr. features: {self.dataset.num_features}')
        print(f'nr. classes: {self.dataset.num_classes}')

    def set_db(self):
        self.db = Database(self.db_table)
        self.db.create_table()
        self.db.create_table_results()

    def precalculate_szemeredi_to_db(self, save_live_tensor=True):
        self.new_dataset = apply_szemeredi(self.db, self.dataset, save_live_tensor)

    def divide_dataset(self):
        self.shuffle()
        self.train_dataset, self.test_dataset = divide_dataset(
            self.new_dataset, int(len(self.new_dataset) * self.train_rate))
        if self.verbose:
            print(f'nr. training graphs: {len(self.train_dataset)}')
            print(f'nr. test graphs: {len(self.test_dataset)}')

    def shuffle(self):
        torch.manual_seed(1337)
        # shuffled_dataset = dataset.shuffle()
        random.shuffle(self.new_dataset)

    def load_data(self):
        self.train_loader = DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True)
        self.test_loader = DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=False)
        if self.verbose:
            print(f"nr. train batches:{len(self.train_loader)}")
            print(f"nr. test batches:{len(self.test_loader)}")

    def train(self, test_start, epochs=1, with_szemeredi=False):
        model = GCN(64, self.dataset.num_node_features, self.dataset.num_classes)
        optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
        criterion = torch.nn.CrossEntropyLoss()
        return train(self.train_loader, self.test_loader, model, optimizer, criterion, epochs,
                     self.db, test_start, test_start + self.train_rate, with_szemeredi, self.verbose)

    def model_example(self):
        model = GCN(64, self.dataset.num_node_features, self.dataset.num_classes)
        print(model)

    def cross_validation_run(self, epochs=2, with_szemeredi=True):
        results = []
        for i in range(0, 5):
            self.train_dataset, self.test_dataset = divide_dataset(
                self.new_dataset, int(len(self.new_dataset) * (1 - self.train_rate) * i))
            self.train_loader = DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True)
            self.test_loader = DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=False)
            train_results = self.train((1 - self.train_rate) * i, epochs, with_szemeredi)
            results.append({
                'full': train_results,
                'mu_loss_avg': numpy.average([item['mu_loss'] for item in train_results]),
                'accuracy_avg': numpy.average([item['accuracy'] for item in train_results]),
                'mu_loss_dev': numpy.std([item['mu_loss'] for item in train_results]),
                'accuracy_dev': numpy.std([item['accuracy'] for item in train_results])
            })
            if self.verbose:
                print('Cross Results: Train loss: {results[mu_loss_avg]:.4f} ± {results[mu_loss_dev]:.4}, Test Acc: {results[accuracy_avg]:.4f} ± {results[accuracy_dev]:.4}\n'.format(
                    results=results[-1]))
        full_results = {
            'results': results,
            'mu_loss_avg': numpy.average([item['mu_loss_avg'] for item in results]),
            'accuracy_avg': numpy.average([item['accuracy_avg'] for item in results]),
            'mu_loss_dev': numpy.std([item['mu_loss_avg'] for item in results]),
            'accuracy_dev': numpy.std([item['accuracy_avg'] for item in results])
        }
        if self.verbose:
            print('Total avg\nTrain loss: {results[mu_loss_avg]:.4f} ± {results[mu_loss_dev]:.4}, Test Acc: {results[accuracy_avg]:.4f} ± {results[accuracy_dev]:.4}'.format(
                results=full_results))
        return full_results

    def cross_validation(self, epochs=2, with_szemeredi=True, save_live_tensor=True):
        if with_szemeredi:
            self.precalculate_szemeredi_to_db(save_live_tensor)
        else:
            self.new_dataset = create_plain_dataset(self.dataset)
        self.divide_dataset()
        self.cross_validation_run(epochs, with_szemeredi)
