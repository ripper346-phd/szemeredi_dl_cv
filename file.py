import io
import os
import gzip
from pathlib import Path
import torch


def bytes_to_tensor(bytes_tensor, compressed=False):
    if compressed:
        bytes_tensor = gzip.decompress(bytes_tensor)
    buffer = io.BytesIO(bytes_tensor)
    return torch.load(buffer)


def tensor_to_buffer(tensor, compress=False):
    filename = 'tensor.pt'
    torch.save(tensor, filename)
    with open(filename, 'rb') as f:
        if compress:
            buffer = io.BytesIO(gzip.compress(f.read(), 5))
        else:
            buffer = io.BytesIO(f.read())
    os.remove(filename)
    return buffer


class DatasetFileDrive:
    def __init__(self, dataset=''):
        """
        Initialize database connection.
        """
        self.folder = './drive/My Drive/szemeredi/runs/{}'.format(dataset)
        Path(self.folder).mkdir(parents=True, exist_ok=True)

    def save(self, tensor_id, tensor, run=1):
        Path('{}/{}'.format(self.folder, run)).mkdir(parents=True, exist_ok=True)
        torch.save(tensor, '{}/{}/{}'.format(self.folder, run, tensor_id))

    def get(self, tensor_id, run=1):
        return torch.load('{}/{}/{}'.format(self.folder, run, tensor_id))

    def exists(self, tensor_id, run=1):
        return os.path.isfile('{}/{}/{}'.format(self.folder, run, tensor_id))
