from torch_geometric.datasets import TUDataset
from dataset_application import DatasetApplication


class DobsonDoig(DatasetApplication):
    def __init__(self, verbose=False):
        dataset = TUDataset(root='data/TUDataset', name='DD')
        super().__init__(dataset, 'dobson-doig', 1, verbose)
