# Other
import matplotlib.pyplot as plt
import networkx as nx
import torch
import numpy as np
import torch_geometric.utils as utils


def to_adj(graph):
    """ Converts pyG graph in np adj_mat for szemeredi"""
    #adj = nx.adjacency_matrix(utils.to_networkx(data)).todense()  # Slow
    adj = np.zeros((graph.num_nodes, graph.num_nodes))
    indices = graph.edge_index
    adj[indices[0], indices[1]] = 1.
    return adj

def viz_graph(graph):
    """ Visualize pyG graph """
    G = utils.to_networkx(graph, to_undirected=True)

    nx.draw_networkx(G, pos=nx.spring_layout(G, seed=42), with_labels=False,
                         node_color='blue', cmap="Set2")
