from torch_geometric.datasets import TUDataset
from dataset_application import DatasetApplication


class Proteins(DatasetApplication):
    def __init__(self, verbose=False):
        dataset = TUDataset(root='data/TUDataset', name='PROTEINS')
        super().__init__(dataset, 'proteins', 1, verbose)
