import mysql.connector as mariadb
import json
from file import tensor_to_buffer, bytes_to_tensor


class Database:
    def __init__(self, table_name=''):
        """
        Initialize database connection.
        """
        self.db_connection = Database.connect()
        self.table_name = table_name

    @staticmethod
    def connect():
        """
        Open a connection to database.
        :return: connection to database
        """
        return mariadb.connect(user='szemeredi', passwd='cMRwE79hG5BxTfmj', db='szemeredi-datasets', host="vps.hastareader.com", port=43346)

    def disconnect(self):
        """
        Close database connection.
        """
        self.db_connection.close()

    def create_table(self):
        """
        Check and create table for dataset
        """
        sql = """
        CREATE TABLE IF NOT EXISTS `{table_name}` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` longtext COLLATE utf8_bin DEFAULT NULL,
          `original_edge_idx` longblob DEFAULT NULL,
          `edge_idx1` longblob DEFAULT NULL,
          `edge_idx2` longblob DEFAULT NULL,
          `edge_idx3` longblob DEFAULT NULL,
          `edge_idx4` longblob DEFAULT NULL,
          `edge_idx5` longblob DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `name` (`name`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
        """.format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute('SET FOREIGN_KEY_CHECKS=0')
        cursor.execute(sql)
        self.db_connection.commit()
        cursor.close()

    def create_table_results(self):
        """
        Check and create table for dataset
        """
        sql = """
        CREATE TABLE IF NOT EXISTS `{table_name}_results` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `epoch` int(11) NOT NULL,
          `test_start` decimal(8, 4) NOT NULL,
          `test_end` decimal(8, 4) NOT NULL,
          `mu_loss` decimal(8, 4) NULL,
          `accuracy` decimal(8, 4) NULL,
          `with_szemeredi` boolean DEFAULT 0 NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
        """.format(table_name=self.table_name)
        sql_view = """
        CREATE VIEW IF NOT EXISTS `{table_name}_results_avgs` AS
        (SELECT with_szemeredi, test_start, test_end, COUNT(epoch) AS count_epochs,
            AVG(mu_loss) AS avg_mu_loss, STDDEV(mu_loss) AS std_mu_loss,
            AVG(accuracy) AS avg_accuracy, STDDEV(accuracy) AS std_accuracy
        FROM `{table_name}_results`
        GROUP BY with_szemeredi, test_start
        ORDER BY with_szemeredi, test_start)
        UNION
        (SELECT with_szemeredi, 'full', 'full', COUNT(test_start), AVG(avg_mu_loss),
            STDDEV(avg_mu_loss), AVG(avg_accuracy), STDDEV(avg_accuracy)
        FROM (SELECT with_szemeredi, test_start, AVG(mu_loss) AS avg_mu_loss, AVG(accuracy) AS avg_accuracy
            FROM `{table_name}_results` GROUP BY with_szemeredi, test_start) avgs
        GROUP BY with_szemeredi
        ORDER BY with_szemeredi)
        """.format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute('SET FOREIGN_KEY_CHECKS=0')
        cursor.execute(sql)
        cursor.execute(sql_view)
        self.db_connection.commit()
        cursor.close()

    def truncate_results(self, del_type='all'):
        """
        Reset results table
        """
        if del_type == 'szemeredi':
            sql = """DELETE FROM `{table_name}_results` WHERE with_szemeredi = 1""".format(table_name=self.table_name)
        elif del_type == 'not_szemeredi':
            sql = """DELETE FROM `{table_name}_results` WHERE with_szemeredi = 0""".format(table_name=self.table_name)
        else:
            sql = """TRUNCATE TABLE `{table_name}_results`""".format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute('SET FOREIGN_KEY_CHECKS=0')
        cursor.execute(sql)
        self.db_connection.commit()
        cursor.close()

    def save_tensor(self, tensor_id, name, original_tensor, tensor, run_number):
        sql = "INSERT INTO `{table_name}`(`id`, `name`, `original_edge_idx`, `edge_idx{run_number}`) VALUES (%s, %s, %s, %s)".format(
            table_name=self.table_name, run_number=int(run_number))
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (tensor_id, name, tensor_to_buffer(
            original_tensor, True).read(), tensor_to_buffer(tensor, True).read()))
        self.db_connection.commit()
        cursor.close()

    def update_tensor(self, tensor_id, tensor, run_number):
        sql = "UPDATE `{table_name}` SET `edge_idx{run_number}` = %s WHERE `id` = %s".format(
            table_name=self.table_name, run_number=int(run_number))
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (tensor_to_buffer(tensor, True).read(), tensor_id))
        self.db_connection.commit()
        cursor.close()

    def check_tensor_id(self, id_tensor):
        sql = "SELECT `id` FROM `{table_name}` WHERE `id` = %s".format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (id_tensor,))
        results = cursor.fetchone()
        cursor.close()
        return results != None

    def get_tensor_id(self, name, original_tensor):
        sql = "SELECT `id` FROM `{table_name}` WHERE `name` = %s".format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (name,))
        results = cursor.fetchall()
        if results and len(results) > 1:
            sql = "SELECT `id` FROM `{table_name}` WHERE `name` = %s AND `original_edge_idx` = %s".format(
                table_name=self.table_name)
            cursor.execute(sql, (name, tensor_to_buffer(original_tensor).read()))
            results = cursor.fetchall()
        cursor.close()
        return False if not results else results[0][0]

    def get_szemeredi_by_id(self, id, run_number):
        sql = "SELECT `edge_idx{run_number}` FROM `{table_name}` WHERE `id` = %s".format(
            table_name=self.table_name, run_number=int(run_number))
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (id,))
        res = cursor.fetchone()
        cursor.close()
        return False if not res else bytes_to_tensor(res[0], True)

    def create_full_table_results(self):
        """
        Check and create table for dataset
        """
        sql = """
        CREATE TABLE IF NOT EXISTS `__cross_validation_results` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` longtext COLLATE utf8_bin DEFAULT NULL,
          `with_szemeredi` boolean DEFAULT false,
          `results` json DEFAULT NULL,
          `notes` text DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
        """.format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute('SET FOREIGN_KEY_CHECKS=0')
        cursor.execute(sql)
        self.db_connection.commit()
        cursor.close()

    def save_full_results(self, name, results, with_szemeredi=False, notes=None):
        sql = "INSERT INTO `__cross_validation_results`(`name`, `with_szemeredi`, `results`, `notes`) VALUES (%s, %s, %s, %s)"
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (name, with_szemeredi, json.dumps(results), notes))
        self.db_connection.commit()
        cursor.close()

    def get_epochs(self, test_start, test_end, with_szemeredi=False):
        sql = """
        SELECT epoch FROM `{table_name}_results` WHERE test_start = %s AND test_end = %s AND with_szemeredi = %s
        """.format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (id,))
        res = cursor.fetchall()
        cursor.close()
        return [] if not res else [row[0] for row in res]

    def save_epoch_results(self, epoch, test_start, test_end, mu_loss, accuracy, with_szemeredi=False):
        sql = """
        INSERT INTO `{table_name}_results`(`epoch`, `test_start`, `test_end`, `mu_loss`, `accuracy`, `with_szemeredi`)
        VALUES (%s, %s, %s, %s, %s, %s)""".format(table_name=self.table_name)
        cursor = self.db_connection.cursor()
        cursor.execute(sql, (epoch, test_start, test_end, mu_loss, accuracy, with_szemeredi))
        self.db_connection.commit()
        cursor.close()
