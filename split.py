def divide_dataset(dataset, test_start):
    test_len = int(len(dataset) * 0.2)
    test_end = test_len + test_start
    if test_start == 0:
        test_dataset = dataset[:test_end]
        train_dataset = dataset[test_end:]
    elif test_end >= len(dataset) - 1:
        test_dataset = dataset[test_start + 1:]
        train_dataset = dataset[:test_start + 1]
    else:
        test_dataset = dataset[test_start:test_end]
        train_dataset = dataset[:test_start] + dataset[test_end:]
    return (train_dataset, test_dataset)
