from torch_geometric.datasets import TUDataset
import torch_geometric.utils as utils
from torch_geometric.data import Data

import sys
sys.path.append("./szecompress")  # Fixes path problems
from szecompress.codec import Codec

def show_szedata():
    # Gets pyG data object
    dataset = TUDataset(root='data/TUDataset', name='PROTEINS')
    data = dataset[2]  # Get the i-th graph object.

    # Calculate compression and decompression
    adj = to_adj(data)
    cdc = Codec(0, 0.5, 20)
    cdc.verbose = False
    k, epsilon, classes, sze_idx, reg_list, nirr = cdc.compress(adj, 'indeg_guided')
    sze = cdc.decompress(adj, 0, classes, k, reg_list)

    # Re-create edge list
    ones = np.nonzero(sze)
    edge_idx = torch.tensor([ones[0] ,ones[1]], dtype=torch.long)
    sze_data = Data(x=data.x, edge_index=edge_idx, y=data.y)

    viz_graph(sze_data)
