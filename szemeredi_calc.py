import hashlib
import base64
import numpy as np
import torch
from torch_geometric.data.data import Data as TGData
from torch_geometric.data.batch import Batch
from torch_geometric.data import DataLoader
from file import tensor_to_buffer, bytes_to_tensor, DatasetFileDrive
from utils import to_adj
from szecompress.codec import Codec


class SzeData(TGData):
    def __init__(self, x=None, edge_index=None, edge_attr=None, y=None,
                 pos=None, normal=None, face=None, **kwargs):
        super(SzeData, self).__init__(x, edge_index, edge_attr, y, pos, normal, face)
        self.szemeredi_id = None
        self.szemeredi = None

    @staticmethod
    def cast_from(data):
        return SzeData(data['x'], data['edge_index'], data['edge_attr'], data['y'], data['pos'], data['normal'], data['face'])


def apply_szemeredi(db, dataset, save_live_tensor=False):
    new_dataset = []
    i = 0
    for data in dataset:
        print(i)
        i += 1
        new_data = SzeData.cast_from(data)
        search_tensor(db, data, i)
        new_data.szemeredi_id = i
        if save_live_tensor:
            new_data.szemeredi = db.get_szemeredi_by_id(i, 1)
        new_dataset.append(new_data)
    return new_dataset


def create_plain_dataset(dataset):
    new_dataset = []
    for data in dataset:
        new_data = SzeData.cast_from(data)
        new_dataset.append(new_data)
    return new_dataset


def search_tensor(db, data, tensor_id, save_name=False, save_original=False):
    if not db.check_tensor_id(tensor_id):
        print('saving to db')
        name = None
        if save_name:
            data_buffer = tensor_to_buffer(data.edge_index).read()
            m = hashlib.sha256()
            m.update(data_buffer)
            name = base64.encodebytes(m.digest())
        original = None
        if save_original:
            original = data.edge_index
        szemeredi_tensor = calc_edge_idx(data)
        db.save_tensor(tensor_id, name, original, szemeredi_tensor, 1)
        for i in range(2, 6):
            szemeredi_tensor = calc_edge_idx(data)
            db.update_tensor(tensor_id, szemeredi_tensor, i)
    return tensor_id


def apply_szemeredi_file(datasetFileDrive, dataset, save_live_tensor=False):
    new_dataset = []
    i = 0
    for data in dataset:
        print(i)
        i += 1
        new_data = SzeData.cast_from(data)
        search_tensor_file(datasetFileDrive, data, i)
        new_data.szemeredi_id = i
        if save_live_tensor:
            new_data.szemeredi = datasetFileDrive.get(i, 1)
            print(new_data.szemeredi)
        new_dataset.append(new_data)
    return new_dataset


def search_tensor_file(datasetFileDrive, data, tensor_id, save_name=False, save_original=False):
    for i in range(1, 6):
        if not datasetFileDrive.exists(tensor_id, i):
            szemeredi_tensor = calc_edge_idx(data)
            datasetFileDrive.save(tensor_id, szemeredi_tensor, i)
    return tensor_id


def calc_edge_idx(data):
    adj = to_adj(data)
    cdc = Codec(0, 0.5, 20)
    cdc.verbose = False
    k, epsilon, classes, sze_idx, reg_list, nirr = cdc.compress(adj, 'indeg_guided')
    sze = cdc.decompress(adj, 0, classes, k, reg_list)
    ones = np.nonzero(sze)
    edge_idx = torch.tensor([ones[0], ones[1]], dtype=torch.long)
    return edge_idx
