def train(train_loader, test_loader, model, optimizer, criterion, epochs, db=None, test_start=0, test_end=1, with_szemeredi=True, verbose=False):
    results = []
    for epoch in range(0, epochs):
        mu_loss = 0
        for data in train_loader:
            out = model(data.x, data.edge_index, data.batch)
            loss = criterion(out, data.y)

            if with_szemeredi:
                # Calculate compression and decompression
                if data.to_data_list()[0].szemeredi == None:
                    edge_idx = db.get_szemeredi_by_id(int(data.to_data_list()[0].szemeredi_id[0]), 1)
                else:
                    edge_idx = data.to_data_list()[0].szemeredi
                out_sze = model(data.x, edge_idx, data.batch)
                loss += criterion(out_sze, data.y)

            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
            mu_loss += loss.item()
        mu_loss /= len(train_loader.dataset)

        model.eval()
        correct = 0
        for data in test_loader:  # Iterate in batches over the training/test dataset.
            out = model(data.x, data.edge_index, data.batch)
            pred = out.argmax(dim=1)  # Use the class with highest probability.
            correct += int((pred == data.y).sum())  # Check against ground-truth labels.
        correct /= len(test_loader.dataset)  # Derive ratio of correct predictions.

        if verbose:
            print(f'Epoch: {epoch:03d}, Train loss: {mu_loss:.4f} Test Acc: {correct:.4f}')
        results.append({'epoch': epoch, 'mu_loss': mu_loss, 'accuracy': correct})
        if db:
            db.save_epoch_results(epoch, test_start, test_end, mu_loss, correct, with_szemeredi)
    return results
